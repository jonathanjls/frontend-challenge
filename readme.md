* This is an application developed for a frontend test of Alpes.one recruitment process.

## Compiling ##
**To compile this application you will need the following libraries/components:**

* Node.js (https://nodejs.org/)
* Bower (Type `npm install bower -g` after node installation)
* Gulp (Type `npm install gulp -g` after node installation)
* Then run `npm install` and `bower install`

## Running ##
**To run this application, type `gulp serve` on console (You must be at project's root)**

* The application will run and open your browser automatically. If it doesn't happen, open your browser in `http://localhost:9000`.