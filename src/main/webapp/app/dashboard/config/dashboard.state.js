(function () {
    'use strict';

    angular
        .module('FrontendChallengeApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    /**
     * States provider for dashboard module
     *
     * @param $stateProvider
     */
    function stateConfig($stateProvider) {
        $stateProvider.state('marketing-analytics', {
            url: '/',
            data: {
                pageTitle: 'Marketing Analytics'
            },
            views: {
                'content@': {
                    templateUrl: 'app/dashboard/view/marketing-analytics.html',
                    controller: 'MarketingAnalyticsController',
                    controllerAs: 'vm'
                }
            }
        });
    }
})();
