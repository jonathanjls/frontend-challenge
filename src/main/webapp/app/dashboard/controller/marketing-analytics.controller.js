(function () {
    'use strict';

    angular
        .module('FrontendChallengeApp')
        .controller('MarketingAnalyticsController', MarketingAnalyticsController);

    MarketingAnalyticsController.$inject = ['DashboardService', 'Toast', '$scope'];

    /**
     * Controller that handles marketing analytics dashboard feature
     *
     * @param DashboardService - Service that get dashboard features data
     * @param Toast - Toast helper
     * @param $scope
     * @constructor
     */
    function MarketingAnalyticsController(DashboardService, Toast, $scope) {
        var vm = this;

        vm.userLocationForm = {};
        vm.getUserLocationByDate = getUserLocationByDate;
        vm.createUsersSessionsChart = createUsersSessionsChart;
        vm.countriesList = [];
        vm.devicesAccessChartData = [];
        vm.userLocationMapChartData = [];
        vm.perMinuteAccessChartData = [{
            key: 'Acessos por minuto',
            values: []
        }];
        vm.perSecondAccessChartData = [{
            key: 'Acessos por segundo',
            values: []
        }];
        vm.userLocationBarChartData = [{
            key: 'Sessões por país',
            values: []
        }];
        vm.primaryBlue = '#2c2f91';
        vm.secondaryBlue = '#00ceff';
        vm.lightBlue = '#297dd7';

        onInit();

        /**
         * Method calls everything that is needed to load on controller init
         */
        function onInit() {
            vm.userLocationForm.userLocationDate = null;
            vm.userLocationForm.showValuesGreaterThan1 = false;
            getCountriesList();
            createUsersSessionsChart();
            createDayHourAccessChart();
            createDevicesAccessChart();
            createPerMinuteAccessChart();
            addMapChartHanders();
        }

        /**
         * Retrieve a list of countries with its names and codes
         */
        function getCountriesList() {
            DashboardService.getCountriesList().then(function (result) {
                for (var i = 0; i < result.data.length; i++) {
                    vm.countriesList[result.data[i].alpha2Code] = result.data[i].name;
                }
            }, function (error) {
                Toast.error('Não foi possível obter a lista de países.');
            });
        }

        /**
         * Create day hour site access chart
         */
        function createDayHourAccessChart() {
            vm.dayHourAccessChartData = DashboardService.getDayHourAccessData();
            vm.dayHourAccessChartOptions = {
                chart: {
                    type: 'lineWithFocusChart',
                    height: 300,
                    margin: {
                        top: 20,
                        right: 30,
                        bottom: 60,
                        left: 65
                    },
                    showControls: false,
                    x: function (d) { return d[0]; },
                    y: function (d) { return d[1]; },
                    color: d3.scale.category10().range(),
                    duration: 300,
                    useInteractiveGuideline: true,
                    xAxis: {
                        axisLabel: 'Hora do Dia',
                        tickFormat: function (d) {
                            return (d < 10 ? '0' + d : d) + ':00:00';
                        },
                        staggerLabels: false
                    },
                    x2Axis: {
                        tickFormat: function (d) {
                            return (d < 10 ? '0' + d : d) + ':00';
                        }
                    },
                    yAxis: {
                        axisLabel: 'Quantidade de Acessos',
                        tickFormat: function (d) {
                            return d;
                        }
                    }
                }
            };
        }

        /**
         * Get devices access statistics and creates devices chart
         */
        function createDevicesAccessChart() {
            // Getting devices access statistics
            DashboardService.getDevicesAccessStatistics().then(function (result) {
                vm.devicesAccessChartData.push({
                    key: 'Computador',
                    y: +result.data.pc.count,
                    color: '#2c2f91'
                });
                vm.devicesAccessChartData.push({
                    key: 'Tablet',
                    y: +result.data.ps3.count,
                    color: '#00ceff'
                });
                vm.devicesAccessChartData.push({
                    key: 'Celular',
                    y: +result.data.xbox.count,
                    color: '#297dd7'
                });
                createDevicesAccessChartOptions();
            }, function (error) {
                Toast.error('Não foi possível obter as estatísticas de acesso por dispositivo.');
            });
        }

        /**
         * Create device access statistics chart options
         * When it is created, chart is renderized
         */
        function createDevicesAccessChartOptions() {
            vm.devicesAccessChartOptions = {
                chart: {
                    type: "pieChart",
                    height: 250,
                    x: function (d) { return d.key; },
                    y: function (d) { return d.y; },
                    showLabels: false,
                    showLegend: false,
                    duration: 500,
                    labelThreshold: 0.01,
                    donut: true,
                    legend: {
                        margin: {
                            top: 0,
                            right: 0,
                            bottom: 0,
                            left: 0
                        }
                    }
                }
            };
        }

        /**
         * Method to get users access location data
         *
         * @param {boolean} isValid
         */
        function getUserLocationByDate(isValid) {
            if (!isValid) {
                $scope.$broadcast('show-errors-check-validity', 'vm.form.userLocationForm');
                Toast.error('Campos não preenchidos corretamente');
                return false;
            }
            DashboardService.getUserLocationByDate(vm.userLocationForm.userLocationDate).then(function (result) {
                vm.result = result.data;
                createUserLocationChartData(result.data);
            }, function (error) {
                Toast.error('Could not find your location');
            });
        }

        /**
         * Create properly object data to user location access charts
         *
         * @param {Object} data
         */
        function createUserLocationChartData(data) {
            var total = 0;
            vm.userLocationMapChartData = [];
            vm.userLocationBarChartData[0].values = [];
            for (var area in data.rates) {
                total += data.rates[area] * 1000;
            }
            for (var area in data.rates) {
                /*
                 * If 'Mostrar somente valores iguais ou maiores que 1%' is checked,
                 * only values greater than 1 % are considered
                 */
                if (vm.userLocationForm.showValuesGreaterThan1) {
                    if ((((data.rates[area] * 1000) / total) * 100) < 1) {
                        continue;
                    }
                }
                // Heatmap chart data
                vm.userLocationMapChartData.push({
                    id: area.substr(0, 2),
                    value: data.rates[area] * 1000
                });
                // Bar chart data
                vm.userLocationBarChartData[0].values.push({
                    label: vm.countriesList[area.substr(0, 2)],
                    value: (data.rates[area] * 1000) / total
                });
            }
            createUserLocationMapChart();
            createUserLocationBarChart();
        }

        /**
         * Create heat map chart object in view
         */
        function createUserLocationMapChart() {
            AmCharts.makeChart("users-access-location-map", {
                type: "map",
                theme: "light",
                colorSteps: 10,
                dataProvider: {
                    map: "worldLow",
                    colorRanges: [{
                        start: 0,
                        end: 10000,
                        color: "#5fafff",
                        variation: -0.01
                    }, {
                        start: 10001,
                        end: 100000,
                        color: "#0080FF",
                        variation: -0.01
                    }, {
                        start: 100001,
                        end: 1000000,
                        color: "#0064c7",
                        variation: -0.01
                    }, {
                        start: 1000001,
                        end: 15620000,
                        color: "#00458a",
                        variation: -0.01
                    }],
                    areas: vm.userLocationMapChartData
                },
                areasSettings: {
                    autoZoom: true,
                    balloonText: "Acessos na data em [[title]]: <b>[[value]]</b>"
                },
                export: {
                    enabled: true
                }
            });
        }

        /**
         * Create user location acces bar chart options
         */
        function createUserLocationBarChart() {
            vm.userLocationBarChartOptions = {
                chart: {
                    type: "multiBarHorizontalChart",
                    height: vm.userLocationForm.showValuesGreaterThan1 ? 300 : 1000,
                    showControls: false,
                    showLegend: false,
                    showValues: true,
                    duration: 500,
                    x: function (d) { return d.label.length > 10 ? d.label.substring(0, 8) + '...' : d.label; },
                    y: function (d) { return d.value; },
                    valueFormat: function (d) {
                        return d3.format('.4%')(d);
                    },
                    tooltip: {
                        valueFormatter: function (d, i) { return d3.format('.4%')(d); }
                    },
                    transitionDuration: 500,
                    xAxis: {
                        showMaxMin: false,
                        fontSize: 10,
                    }
                }
            };
        }

        /**
         * Add necessary handlers to configure users access location heat map chart
         */
        function addMapChartHanders() {
            // Handler to configure color ranges in the users access location heat map chart
            AmCharts.addInitHandler(function (chart) {
                var dataProvider = chart.dataProvider;
                var areas = chart.dataProvider.areas;
                var colorRanges = dataProvider.colorRanges;

                function ColorLuminance(hex, lum) {
                    // validate hex string
                    hex = String(hex).replace(/[^0-9a-f]/gi, '');
                    if (hex.length < 6) {
                        hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
                    }
                    lum = lum || 0;
                    // convert to decimal and change luminosity
                    var rgb = "#", c, i;
                    for (i = 0; i < 3; i++) {
                        c = parseInt(hex.substr(i * 2, 2), 16);
                        c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
                        rgb += ("00" + c).substr(c.length);
                    }
                    return rgb;
                }

                if (colorRanges) {
                    var item, range, average, variation;
                    for (var i = 0, iLen = areas.length; i < iLen; i++) {
                        item = areas[i];
                        for (var x = 0, xLen = colorRanges.length; x < xLen; x++) {
                            range = colorRanges[x];
                            if (item.value >= range.start && item.value <= range.end) {
                                average = (range.start - range.end) / 2;
                                if (item.value <= average)
                                    variation = (range.variation * -1) / item.value * average;
                                else if (item.value > average)
                                    variation = range.variation / item.value * average;
                                item.color = ColorLuminance(range.color, variation.toFixed(2));
                            }
                        }
                    }
                }
            }, ["map"]);
        }

        /**
         * Create active users/sessions access line chart data
         */
        function getUsersSessionsChartData() {
            var users = [], sessions = [];
            var usersSessionsData = DashboardService.getUsersSessionsData();
            var numberOfDays = vm.usersSessionsPeriod || 7;
            for (var i = 0; i < numberOfDays; i++) {
                var xVal = usersSessionsData[i].date.split('-')[2];
                users.push({ x: xVal, y: usersSessionsData[i].users });
                sessions.push({ x: xVal, y: usersSessionsData[i].sessions });
            }
            vm.usersSessionsChartData = [{
                values: users,
                key: 'Usuários',
                color: '#252465'
            }, {
                values: sessions,
                key: 'Sessões',
                color: '#52c4ff'
            }];
        }

        /**
         * Create active users/sessions access line chart options
         */
        function createUsersSessionsChart() {
            getUsersSessionsChartData();
            vm.usersSessionsChartOptions = {
                chart: {
                    type: "lineChart",
                    useInteractiveGuideline: true,
                    dispatch: {},
                    xAxis: {
                        axisLabel: "Dia"
                    },
                    yAxis: {
                        axisLabel: "Quantidade Usuários/Sessões",
                        axisLabelDistance: -10
                    }
                }
            };
        }

        /**
         * Create per-minute access bar chart
         */
        function createPerMinuteAccessChart() {
            // Getting per-minute access statistics
            DashboardService.getPerMinuteAccessStatistics().then(function (result) {
                vm.perMinuteAccessChartData[0].values.push({
                    label: 'Home',
                    value: +result.data.ps4.count
                });
                vm.perMinuteAccessChartData[0].values.push({
                    label: 'Sobre nós',
                    value: +result.data.xone.count
                });
                vm.perSecondAccessChartData[0].values.push({
                    label: 'Home',
                    value: +result.data.ps4.count / 60
                });
                vm.perSecondAccessChartData[0].values.push({
                    label: 'Sobre nós',
                    value: +result.data.xone.count / 60
                });
                createPerMinuteAccessChartOptions();
                createPerSecondAccessChartOptions();
            }, function (error) {
                Toast.error('Não foi possível obter as estatísticas de acesso por minuto/segundo.');
            });
        }

        /**
         * Create per-minute access bar chart options
         */
        function createPerMinuteAccessChartOptions() {
            vm.perMinuteAccessChartOptions = {
                chart: {
                    type: "discreteBarChart",
                    height: 300,
                    showControls: false,
                    showLegend: false,
                    showValues: true,
                    duration: 500,
                    x: function (d) { return d.label.length > 10 ? d.label.substring(0, 8) + '...' : d.label; },
                    y: function (d) { return d.value; },
                    valueFormat: function (d) {
                        return d;
                    },
                    tooltip: {
                        valueFormatter: function (d, i) { return d; }
                    },
                    transitionDuration: 500,
                    xAxis: {
                        showMaxMin: false,
                        fontSize: 10,
                    }
                }
            };
        }

        /**
         * Create per-second access bar chart options
         */
        function createPerSecondAccessChartOptions() {
            vm.perSecondAccessChartOptions = {
                chart: {
                    type: "discreteBarChart",
                    height: 300,
                    showControls: false,
                    showLegend: false,
                    showValues: true,
                    duration: 500,
                    x: function (d) { return d.label.length > 10 ? d.label.substring(0, 8) + '...' : d.label; },
                    y: function (d) { return d.value; },
                    forceY: [
                        1000
                    ],
                    valueFormat: function (d) {
                        return d3.format('.2f')(d);
                    },
                    tooltip: {
                        valueFormatter: function (d, i) { return d3.format('.2f')(d); }
                    },
                    transitionDuration: 500,
                    xAxis: {
                        showMaxMin: false,
                        fontSize: 10,
                    }
                }
            };
        }

    }
})();
