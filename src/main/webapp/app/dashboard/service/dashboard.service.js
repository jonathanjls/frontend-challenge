(function () {
    'use strict';

    angular
        .module('FrontendChallengeApp')
        .factory('DashboardService', DashboardService);

    DashboardService.$inject = ['$http', 'USER_LOCATION_ACCESS_API_HOST', 'COUNTRIES_LIST_API_HOST', 'DEVICES_STATISTICS_API_HOST'];

    /**
     * Dashboard service to handle requests
     *
     * @param $http
     * @param USER_LOCATION_ACCESS_API_HOST
     * @returns {Object}
     * @constructor
     */
    function DashboardService($http, USER_LOCATION_ACCESS_API_HOST, COUNTRIES_LIST_API_HOST, DEVICES_STATISTICS_API_HOST) {
        var service = {
            getUserLocationByDate: getUserLocationByDate,
            getCountriesList: getCountriesList,
            getDevicesAccessStatistics: getDevicesAccessStatistics,
            getUsersSessionsData: getUsersSessionsData,
            getPerMinuteAccessStatistics: getPerMinuteAccessStatistics,
            getDayHourAccessData: getDayHourAccessData
        };

        return service;

        /**
         * Get users access location data by date
         *
         * @param date
         * @returns {*}
         */
        function getUserLocationByDate(date) {
            date = angular.isUndefined(date) || !date ? new Date() : date;
            var month = (date.getMonth() + 1);
            var day = (date.getDate() + 1);
            if (month < 10) {
                month = '0' + month;
            }
            if (day < 10) {
                day = '0' + day;
            }
            return $http.jsonp(USER_LOCATION_ACCESS_API_HOST + '/' + date.getFullYear() + '-' + month + '-' + day);
        }

        /**
         * Retrieve a list with country names and its codes
         */
        function getCountriesList() {
            return $http.get(COUNTRIES_LIST_API_HOST + '/all?fields=name;alpha2Code');
        }

        /**
         * Retrieve device access statistics from service
         */
        function getDevicesAccessStatistics() {
            return $http.get(DEVICES_STATISTICS_API_HOST + '/onlinePlayers?output=json');
        }

        /**
         * Retrieve per-minute access statistics from service
         */
        function getPerMinuteAccessStatistics() {
            return $http.get(DEVICES_STATISTICS_API_HOST + '/onlinePlayers?output=json');
        }

        /**
         * Get number of active users/sessions by day
         *
         * TODO: MOCKED DATA. Get data from service
         */
        function getUsersSessionsData() {
            return [{
                date: '2017-08-18',
                users: 5664,
                sessions: 4105
            }, {
                date: '2017-08-17',
                users: 3679,
                sessions: 6777
            }, {
                date: '2017-08-16',
                users: 4384,
                sessions: 6016
            }, {
                date: '2017-08-15',
                users: 11369,
                sessions: 9225
            }, {
                date: '2017-08-14',
                users: 7205,
                sessions: 9015
            }, {
                date: '2017-08-13',
                users: 4454,
                sessions: 8769
            }, {
                date: '2017-08-12',
                users: 8617,
                sessions: 4593
            }, {
                date: '2017-08-11',
                users: 7043,
                sessions: 4445
            }, {
                date: '2017-08-10',
                users: 11975,
                sessions: 5560
            }, {
                date: '2017-08-09',
                users: 7545,
                sessions: 7661
            }, {
                date: '2017-08-08',
                users: 6713,
                sessions: 6393
            }, {
                date: '2017-08-07',
                users: 8725,
                sessions: 5689
            }, {
                date: '2017-08-06',
                users: 3972,
                sessions: 7027
            }, {
                date: '2017-08-05',
                users: 10538,
                sessions: 3254
            }, {
                date: '2017-08-04',
                users: 10264,
                sessions: 9761
            }, {
                date: '2017-08-03',
                users: 10964,
                sessions: 5348
            }, {
                date: '2017-08-02',
                users: 6848,
                sessions: 5318
            }, {
                date: '2017-08-01',
                users: 8090,
                sessions: 6908
            }, {
                date: '2017-07-31',
                users: 11187,
                sessions: 4205
            }, {
                date: '2017-07-30',
                users: 3471,
                sessions: 8828
            }, {
                date: '2017-07-29',
                users: 4075,
                sessions: 6135
            }, {
                date: '2017-07-28',
                users: 12215,
                sessions: 4409
            }, {
                date: '2017-07-27',
                users: 3584,
                sessions: 8323
            }, {
                date: '2017-07-26',
                users: 6730,
                sessions: 4489
            }, {
                date: '2017-07-25',
                users: 11294,
                sessions: 7276
            }, {
                date: '2017-07-24',
                users: 11882,
                sessions: 6846
            }, {
                date: '2017-07-23',
                users: 5280,
                sessions: 2597
            }, {
                date: '2017-07-22',
                users: 10064,
                sessions: 9995
            }, {
                date: '2017-07-21',
                users: 8098,
                sessions: 5992
            }, {
                date: '2017-07-20',
                users: 4763,
                sessions: 3065
            }, {
                date: '2017-07-19',
                users: 5083,
                sessions: 4113
            }];
        }

        /**
         * Get day hour site access statistics data
         *
         * TODO: MOCKED DATA. Get data from service
         */
        function getDayHourAccessData() {
            return [
                {
                    key: "Seg",
                    color: '#252465',
                    values: [[0, 441], [1, 198], [2, 268], [3, 271], [4, 331], [5, 365], [6, 90], [7, 180], [8, 210], [9, 310], [10, 264], [11, 451], [12, 152], [13, 466], [14, 28], [15, 164], [16, 173], [17, 286], [18, 404], [19, 246], [20, 174], [21, 32], [22, 132], [23, 151]]
                }, {
                    key: "Ter",
                    color: '#3533c1',
                    values: [[0, 196], [1, 63], [2, 258], [3, 96], [4, 499], [5, 394], [6, 52], [7, 348], [8, 215], [9, 325], [10, 38], [11, 11], [12, 351], [13, 259], [14, 201], [15, 26], [16, 244], [17, 115], [18, 339], [19, 387], [20, 100], [21, 461], [22, 80], [23, 20]]
                }, {
                    key: "Qua",
                    color: '#1da4f3',
                    values: [[0, 286], [1, 59], [2, 151], [3, 331], [4, 94], [5, 360], [6, 394], [7, 177], [8, 384], [9, 360], [10, 171], [11, 185], [12, 297], [13, 316], [14, 379], [15, 241], [16, 458], [17, 9], [18, 72], [19, 89], [20, 67], [21, 22], [22, 201], [23, 226]]
                }, {
                    key: "Qui",
                    color: '#78cdff',
                    values: [[0, 205], [1, 374], [2, 459], [3, 55], [4, 460], [5, 213], [6, 271], [7, 137], [8, 84], [9, 155], [10, 470], [11, 417], [12, 387], [13, 422], [14, 253], [15, 5], [16, 401], [17, 180], [18, 147], [19, 171], [20, 382], [21, 162], [22, 282], [23, 318]]
                }, {
                    key: "Sex",
                    color: '#aa7cf3',
                    values: [[0, 392], [1, 347], [2, 359], [3, 489], [4, 430], [5, 15], [6, 68], [7, 126], [8, 469], [9, 167], [10, 104], [11, 203], [12, 169], [13, 120], [14, 270], [15, 7], [16, 463], [17, 275], [18, 331], [19, 436], [20, 223], [21, 79], [22, 254], [23, 34]]
                }, {
                    key: "Sáb",
                    color: '#771818',
                    values: [[0, 78], [1, 165], [2, 354], [3, 380], [4, 422], [5, 15], [6, 329], [7, 367], [8, 258], [9, 247], [10, 15], [11, 325], [12, 227], [13, 385], [14, 303], [15, 301], [16, 188], [17, 425], [18, 113], [19, 480], [20, 54], [21, 162], [22, 95], [23, 142]]
                }, {
                    key: "Dom",
                    color: '#77185a',
                    values: [[0, 177], [1, 415], [2, 146], [3, 99], [4, 377], [5, 130], [6, 232], [7, 60], [8, 363], [9, 491], [10, 305], [11, 451], [12, 384], [13, 134], [14, 254], [15, 287], [16, 481], [17, 121], [18, 250], [19, 172], [20, 304], [21, 376], [22, 136], [23, 489]]
                }
            ];
        }
    }
})();
