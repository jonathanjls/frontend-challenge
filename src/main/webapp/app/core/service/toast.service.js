(function () {
    'use strict';

    angular.module('FrontendChallengeApp')
        .factory('Toast', Toast);

    Toast.$inject = ['$mdToast'];

    /**
     * Helper service for toasts events handler
     *
     * @param $mdToast
     * @returns {{toast: toast, warning: warning, error: error, info: info, success: success}|*}
     * @constructor
     */
    function Toast($mdToast) {
        var self;

        self = {
            toast: toast,
            warning: warning,
            error: error,
            info: info,
            success: success
        };

        function toast(message, theme, themeIcon) {
            $mdToast.show($mdToast.customToast()
                .textContent(message)
                .position('top right')
                .hideDelay(6000)
                .theme(theme)
                .themeIcon(themeIcon));
        }

        function warning(message) {
            return toast(message, 'warning-toast', 'warning');
        }

        function error(message) {
            return toast(message, 'error-toast', 'error');
        }

        function info(message) {
            return toast(message, 'info-toast', 'info');
        }

        function success(message) {
            return toast(message, 'success-toast', 'done');
        }

        return self;
    }

}());
