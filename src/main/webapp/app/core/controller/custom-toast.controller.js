(function () {
    'use strict';

    angular.module('FrontendChallengeApp')
        .controller('CustomToastController', CustomToastController);

    CustomToastController.$inject = ['$scope', '$mdToast'];

    /**
     * This is a customized controller based on $mdToast default controller for treatment of application custom toast interactions
     *
     * @param $scope
     * @param $mdToast
     * @constructor
     */
    function CustomToastController($scope, $mdToast) {
        var vm = this;
        var textContent = '';
        var isDlgOpen = false;

        if (vm.highlightAction) {
            $scope.highlightClasses = ['md-highlight', vm.highlightClass]
        }

        $scope.$watch(function () {
            return textContent;
        }, function () {
            vm.content = textContent;
        });

        vm.resolve = function () {
            $mdToast.hide('ok');
        };

        $scope.closeToast = function () {
            if (isDlgOpen) return;

            $mdToast
                .hide()
                .then(function () {
                    isDlgOpen = false;
                });
        };
    }

}());
