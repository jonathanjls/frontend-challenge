(function () {
    'use strict';

    angular
        .module('FrontendChallengeApp', [
            'ngResource',
            'ngCookies',
            'ngAnimate',
            'ngMessages',
            'ngSanitize',
            'ngAria',
            'ngCacheBuster',
            'ui.router',
            'ngMaterial',
            'blockUI',
            'nvd3'
        ])
        .config(configSce)
        .config(materialDesignConfig)
        .config(customToastConfig)
        .config(blockUIConfig)
        .config(mdDateLocaleProvider)
        .run();

    configSce.$inject = ['$sceDelegateProvider', 'USER_LOCATION_ACCESS_API_HOST', 'COUNTRIES_LIST_API_HOST', 'DEVICES_STATISTICS_API_HOST'];
    function configSce($sceDelegateProvider, USER_LOCATION_ACCESS_API_HOST, COUNTRIES_LIST_API_HOST, DEVICES_STATISTICS_API_HOST) {
        $sceDelegateProvider.resourceUrlWhitelist([
            // Allow same origin resource loads.
            'self',
            // Allow loading from our assets domain. **.
            USER_LOCATION_ACCESS_API_HOST + '/**',
            COUNTRIES_LIST_API_HOST + '/**',
            DEVICES_STATISTICS_API_HOST + '/**'
        ]);
    }

    customToastConfig.$inject = ['$mdToastProvider'];
    function customToastConfig($mdToastProvider) {
        $mdToastProvider.addPreset('customToast', {
            argOption: 'textContent',
            methods: ['textContent', 'themeIcon', 'content', 'action', 'highlightAction', 'highlightClass', 'theme', 'parent'],
            options: /* ngInject */['$mdToast', '$mdTheming', function ($mdToast, $mdTheming) {
                return {
                    template: '<md-toast md-theme="{{ toast.theme }}" ng-class="{\'md-capsule\': toast.capsule}">' +
                    '<div class="md-toast-content">' +
                    '<md-icon class="icone30 ng-scope material-icons md-light" md-font-set="material-icons">{{ toast.themeIcon }}</md-icon>' +
                    '<span class="md-toast-text" role="alert" aria-relevant="all" aria-atomic="true">{{ toast.textContent }}</span>' +
                    '<md-button class="md-action" ng-if="toast.action" ng-click="toast.resolve()" ng-class="highlightClasses">{{ toast.action }}</md-button>' +
                    '<md-button><md-icon class="icone24 ng-scope material-icons md-light" md-font-set="material-icons" ng-click="closeToast()">close</md-icon></md-button>' +
                    '</div>' +
                    '</md-toast>',
                    controller: 'CustomToastController',
                    theme: $mdTheming.defaultTheme(),
                    controllerAs: 'toast',
                    bindToController: true
                };
            }]
        });
    }

    blockUIConfig.$inject = ['blockUIConfig'];
    function blockUIConfig(blockUIConfig) {
        blockUIConfig.template = '<div class=\"block-ui-overlay\"></div><div class=\"block-ui-message-container\" aria-live=\"assertive\" aria-atomic=\"true\"><div layout="row" layout-sm="column" layout-align="space-around"><md-progress-circular md-mode="indeterminate"></md-progress-circular></div></div>';
        blockUIConfig.requestFilter = function (config) {
            if (config.url.match(/\/api/g)) {
                return true;
            }
        };
    }

    materialDesignConfig.$inject = ['$mdThemingProvider'];
    function materialDesignConfig($mdThemingProvider) {
        var customPrimary = {
            '50': '#f1f1f1',
            '100': '#413fb0',
            '200': '#3a389d',
            '300': '#33318b',
            '400': '#2c2b78',
            '500': '#252465',
            '600': '#1e1d52',
            '700': '#17173f',
            '800': '#10102d',
            '900': '#09091a',
            'A100': '#5e5dc5',
            'A200': '#7170cc',
            'A400': '#8482d3',
            'A700': '#030207',
            'contrastDefaultColor': 'light',
            'contrastDarkColors': ['50', '100', '200', '300', '400', 'A100'],
            'contrastLightColors': 'light'
        };

        var customAccent = {
            '50': '#000a08',
            '100': '#00231d',
            '200': '#003d31',
            '300': '#005646',
            '400': '#00705a',
            '500': '#00896f',
            '600': '#00bc97',
            '700': '#00d6ac',
            '800': '#00efc0',
            '900': '#0affcf',
            'A100': '#00bc97',
            'A200': '#00a383',
            'A400': '#00896f',
            'A700': '#007e66',
            'contrastDefaultColor': 'light',
            'contrastDarkColors': ['50', '100', '200', '300', '400', 'A100'],
            'contrastLightColors': 'light'
        };

        var customWarn = {
            '50': '#f4b5be',
            '100': '#f19faa',
            '200': '#ee8996',
            '300': '#ea7382',
            '400': '#e75c6f',
            '500': '#e4465b',
            '600': '#e13047',
            '700': '#d81f38',
            '800': '#c11c32',
            '900': '#ab192c',
            'A100': '#f7ccd1',
            'A200': '#fbe2e5',
            'A400': '#fef8f9',
            'A700': '#951627',
            'contrastDefaultColor': 'light',
            'contrastDarkColors': ['50', '100', '200', '300', '400', 'A100'],
            'contrastLightColors': 'light'
        };
        $mdThemingProvider.definePalette('customPrimary', customPrimary);
        $mdThemingProvider.definePalette('customAccent', customAccent);
        $mdThemingProvider.definePalette('customWarn', customWarn);
        $mdThemingProvider.theme('default')
            .primaryPalette('customPrimary', {
                'default': '500',
                'hue-1': '50',
                'hue-2': '600',
                'hue-3': '700'
            })
            .accentPalette('customAccent')
            .warnPalette('customWarn');
        $mdThemingProvider.theme('warning-toast');
        $mdThemingProvider.theme('error-toast');
        $mdThemingProvider.theme('info-toast');
        $mdThemingProvider.theme('success-toast');
    }

    mdDateLocaleProvider.$inject = ['$mdDateLocaleProvider'];

    function mdDateLocaleProvider($mdDateLocaleProvider) {
        $mdDateLocaleProvider.months = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
        $mdDateLocaleProvider.shortMonths = ['Jan', 'Fev', 'Mar', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'];
        $mdDateLocaleProvider.days = ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'];
        $mdDateLocaleProvider.shortDays = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'];

        $mdDateLocaleProvider.parseDate = function (dateString) {
            var m = moment(dateString, 'DD/MM/YYYY', true);
            return m.isValid() ? m.toDate() : new Date(NaN);
        };

        $mdDateLocaleProvider.formatDate = function (date) {
            var m = moment(date);
            return m.isValid() ? m.format('DD/MM/YYYY') : '';
        };
    }
})();
